from django.urls import path

from app import views

urlpatterns = [
    path('questions/<int:profile_>', views.all_questions, name='questions'),
    path('hot/<int:profile_>', views.hot, name='hot'),
    path('ask/<int:profile_>', views.ask, name='ask'),
    path('question/<int:profile_>/<int:qid_>/', views.question, name='question'),
    path('settings/<int:profile_>', views.settings, name='profile'),
    path('tag/<int:profile_>/<str:tag_>', views.tag, name='tag'),
]
