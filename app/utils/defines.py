LEN_OF_PRETEXT = 255

NUMBER_OF_ALL_QUESTIONS = 50

NUMBER_OF_ALL_ANSWERS = 10

questions = {
    i: {'id': i,
        'title': f'question # {i}',
        'question': 'How to create calculator on Python?',
        'text': 'In ultricies, magna id vestibulum imperdiet, \
				 mi tortor scelerisque est, in vestibulum ex mauris non ante. \
				 Mauris vitae tortor quis nisi placerat porttitor quis in ex. \
				 Ut leo turpis, mattis in orci nec, scelerisque congue felis. \
				 Duis sollicitudin egestas arcu, at viverra augue pellentesque tristique. \
				 Cras porttitor lacus nisi, sodales efficitur velit tristique id. \
				 Nullam hendrerit interdum facilisis. Cras sed diam sit amet magna pharetra facilisis ac et lacus. \
				 Nam interdum facilisis mi ac luctus. Vivamus at augue quam. \
				 Donec ut condimentum purus. Duis cursus mauris vitae maximus tincidunt. \
                 Nam quis tincidunt dui, non semper eros.Phasellus quis laoreet ex. \
                Quisque aliquam auctor lacus eu iaculis.',
        'tags': ['C++', 'django', 'Python', 'React'],
        }
    for i in range(NUMBER_OF_ALL_QUESTIONS)
}

for i in range(NUMBER_OF_ALL_QUESTIONS):
    questions[i]['pretext'] = questions[i]['text'][:LEN_OF_PRETEXT:]

answers = [
    {
        'id': i,
        'text': 'Fusce neque. Vestibulum suscipit nulla quis orci. \
			Curabitur ullamcorper ultricies nisi. Nullam dictum felis eu pede mollis pretium.  \
			Curabitur at lacus ac velit ornare lobortis. Ut id nisl quis enim dignissim sagittis. \
			Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. \
			Pellentesque habitant morbi tristique senectus et netus.',
    }
    for i in range(NUMBER_OF_ALL_ANSWERS)
]
