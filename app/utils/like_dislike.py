def get_container(cont_type, post_obj, query):
    if not query:
        container = cont_type(content_object=post_obj)
        container.save()
    else:
        try:
            container = cont_type.objects.get(object_id=post_obj.id)
        except (cont_type.MultipleObjectsReturned, cont_type.DoesNotExist) as error:
            query.delete()
            container = cont_type(contaent_object=post_obj)
            container.save()

    return container


def has_dislike_handle(cont_type, post_obj, author):
    try:
        container = cont_type.objects.get(object_id=post_obj.id,)
        if author in container.users.all():
            post_obj.rating += 1
            container.users.remove(author)
            post_obj.save()
            container.save()
    except (cont_type.MultipleObjectsReturned, cont_type.DoesNotExist):
        pass


def has_like_handle(cont_type, post_obj, author):
    try:
        container = cont_type.objects.get(object_id=post_obj.id,)
        if author in container.users.all():
            post_obj.rating -= 1
            container.users.remove(author)
            post_obj.save()
            container.save()
    except (cont_type.MultipleObjectsReturned, cont_type.DoesNotExist):
        pass
