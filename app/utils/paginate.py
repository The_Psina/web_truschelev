from askme.settings import MAX_PAGINATOR_SHOWN_BUTTONS, ITEMS_ON_PAGE
from django.core.paginator import InvalidPage, Paginator


def create_borders(start, stop, page):
    middle = (MAX_PAGINATOR_SHOWN_BUTTONS + 1) // 2
    l1 = list(reversed([(page - i) for i in range(start, middle) if i < page]))
    l2 = [page]
    l3 = [(page + i) for i in range(start, middle) if (i + page) <= stop]
    return l1 + l2 + l3


def paginate(object_list, request):
    num_of_all_buttons = len(object_list) / ITEMS_ON_PAGE
    paginator = Paginator(object_list, ITEMS_ON_PAGE)
    page = paginator.get_page(request.GET.get('page'))

    borders = create_borders(1, num_of_all_buttons, page.number)

    try:
        prev_page = page.previous_page_number()
        next_page = page.next_page_number()
    except InvalidPage:
        prev_page = int(page.previous_page_number()) - 1
        next_page = int(page.next_page_number()) + 1

    result = {
        'borders': borders,
        'prev': prev_page,
        'next': next_page,
        'number': int(page.number)
    }

    return page, result
