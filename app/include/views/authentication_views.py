from django.shortcuts import render


def login(request):
    return render(request, 'login_page.html', {})


def registration(request):
    return render(request, 'registration_page.html', {})
