from django.shortcuts import render
from django.shortcuts import render, get_object_or_404

from app.include.models.Question import Question
from app.include.models.Answer import Answer
from app.models import Tag

from app.utils.paginate import *
from app.utils.defines import *


def question(request, qid_, profile_):
    question = get_object_or_404(Question, id=qid_)
    answers, paginator_range = paginate(
        Answer.objects.get_answer_question(question), request)
    # member_settings = get_object_or_404(Profile, user=profile_)

    context = {
        'question': question,
        'answers': answers,
        'paginator': paginator_range,
    }

    return render(request, 'question.html', context)


def all_questions(request, profile_):
    questions, paginator_range = paginate(Question.objects.all(), request)
    # member_settings = get_object_or_404(Profile, user=profile_)

    context = {
        'questions': questions,
        'paginator': paginator_range,
    }

    return render(request, 'forum.html', context)


def settings(request, profile_):
    # member_settings = get_object_or_404(Profile, user=profile_)
    return render(request, 'settings_page.html', {})


def hot(request, profile_):
    question, paginator_range = paginate(
        Question.objects.all().order_by('-rating'),
        request
    )
    # member_settings = get_object_or_404(Profile, user=profile_)

    context = {
        'question': question,
        'paginator': paginator_range,
    }

    return render(request, 'sorted_questions.html', context)


def tag(request, tag_, profile_):
    tag_on_page = get_object_or_404(Tag, name=tag_)
    questions, paginator_range = paginate(
        Question.objects.get_by_tag(tag_on_page), request)
    # member_settings = get_object_or_404(Profile, user=profile_)

    context = {
        'questions': questions,
        'tag': tag_on_page,
        'paginator': paginator_range,
    }

    return render(request, 'sorted_questions.html', context)


def ask(request, profile_):
    # member_settings = get_object_or_404(Profile, user=profile_)
    return render(request, 'ask_page.html', {})
