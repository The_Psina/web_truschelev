from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericRelation
from django.utils import timezone

from django.db import models

from app.utils.like_dislike import *
from app.models import Like, Dislike

from app.include.models.Question import Question


class AnswerManager(models.Manager):
    def add_answer(self, question_id, author_id, text):
        answer = self.create(author_id=author_id, question_id=question_id, text=text)
        question = Question.objects.get(pk=question_id)
        question.answers += 1
        question.save()
        return answer

    def get_answer_question(self, question):
        return self.filter(question=question)


class Answer(models.Model):
    text = models.TextField(verbose_name='Текст ответа')

    author = models.ForeignKey(
        'app.Profile',
        on_delete=models.DO_NOTHING,
        db_index=True
    )
    question = models.ForeignKey(
        Question,
        on_delete=models.CASCADE,
        db_index=True
    )
    create_date = models.DateTimeField(
        default=timezone.now,
        verbose_name='Время создания вопроса'
    )
    rating = models.SmallIntegerField(
        default=0,
        verbose_name='Рейтинг вопроса'
    )
    dislikes = GenericRelation(Dislike)

    objects = AnswerManager()

    def liked(self, author):
        question = Like.objects.filter(
            content_type=ContentType.objects.get_for_model(Question),
            object_id=self.id
        )
        likes = get_container(Like, self, question)
        has_dislike_handle(Dislike, self, author)

        try:
            if author in likes.users.all():
                print("{}'s already liked".format(author))
                return
            else:
                self.rating += 1
                likes.users.add(author)
                likes.save()
                self.save()
                print("liked")

        except Like.DoesNotExist:
            return

    def disliked(self, author):
        question = Dislike.objects.filter(
            content_type=ContentType.objects.get_for_model(Question),
            object_id=self.id
        )
        dislikes = get_container(Like, self, question)
        has_like_handle(Dislike, self, author)

        try:
            if author in dislikes.users.all():
                print("{}'s already disliked".format(author))
                return
            else:
                self.rating -= 1
                self.dislikes += 1
                dislikes.users.add(author)
                dislikes.save()
                self.save()
                print("disliked")

        except Like.DoesNotExist:
            return

    def refresh_rating(self):
        likes = Like.objects.filter(
            content_type=ContentType.objects.get_for_model(Question),
            object_id=self.id
        )
        dislikes = Dislike.objects.filter(
            content_type=ContentType.objects.get_for_model(Question),
            object_id=self.id
        )

        likes = get_container(Like, self, likes)
        dislikes = get_container(Like, self, dislikes)

        try:
            l_count = likes.users.all().count()
        except Like.DoesNotExist as error:
            l_count = 0
        try:
            d_count = dislikes.users.all().count()
        except Dislike.DoesNotExist as error:
            d_count = 0

        count = l_count + d_count
        rating = ((count - d_count) / count) * 100
        self.rating = rating
        self.dislikes = d_count
        self.save()
        return rating

    def __str__(self):
        return self.text[:10]

    class Meta:
        ordering = ['-create_date']
