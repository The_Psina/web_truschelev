from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericRelation
from django.utils import timezone

from django.db import models

from app.utils.like_dislike import *
from app.models import Like, Dislike

from datetime import datetime


class QuestionManager(models.Manager):
    def get_by_tag(self, tag):
        return self.filter(tags__name=tag)


class Question(models.Model):
    title = models.CharField(max_length=200, verbose_name='Заголовок вопроса')
    text = models.TextField(verbose_name='Текст вопроса')
    tags = models.ManyToManyField('app.Tag')

    author = models.ForeignKey(
        'app.Profile',
        on_delete=models.DO_NOTHING,
        db_index=True
    )
    create_date = models.DateTimeField(
        default=timezone.now,
        verbose_name='Время создания вопроса'
    )
    is_active = models.BooleanField(
        default=True,
        verbose_name='Доступность вопроса'
    )
    answers = models.SmallIntegerField(
        default=0,
        verbose_name='Количество ответов'
    )
    rating = models.SmallIntegerField(
        default=0,
        verbose_name='Рейтинг вопроса'
    )
    dislikes = GenericRelation(Dislike)

    objects = QuestionManager()

    def published_recently(self):
        return self.create_date >= timezone.now() - datetime.timedelta(days=1)

    def liked(self, author):
        question = Like.objects.filter(
            content_type=ContentType.objects.get_for_model(Question),
            object_id=self.id
        )
        likes = get_container(Like, self, question)
        has_dislike_handle(Dislike, self, author)

        try:
            if author in likes.users.all():
                print("{}'s already liked".format(author))
                return
            else:
                self.rating += 1
                likes.users.add(author)
                likes.save()
                self.save()
                print("liked")

        except Like.DoesNotExist:
            return

    def disliked(self, author):
        question = Dislike.objects.filter(
            content_type=ContentType.objects.get_for_model(Question),
            object_id=self.id
        )
        dislikes = get_container(Like, self, question)
        has_like_handle(Dislike, self, author)

        try:
            if author in dislikes.users.all():
                print("{}'s already disliked".format(author))
                return
            else:
                self.rating -= 1
                self.dislikes += 1
                dislikes.users.add(author)
                dislikes.save()
                self.save()
                print("disliked")

        except Like.DoesNotExist:
            return

    def refresh_rating(self):
        likes = Like.objects.filter(
            content_type=ContentType.objects.get_for_model(Question),
            object_id=self.id
        )
        dislikes = Dislike.objects.filter(
            content_type=ContentType.objects.get_for_model(Question),
            object_id=self.id
        )

        likes = get_container(Like, self, likes)
        dislikes = get_container(Like, self, dislikes)

        try:
            l_count = likes.users.all().count()
        except Like.DoesNotExist as error:
            l_count = 0
        try:
            d_count = dislikes.users.all().count()
        except Dislike.DoesNotExist as error:
            d_count = 0

        count = l_count + d_count
        rating = ((count - d_count) / count) * 100
        self.rating = rating
        self.dislikes = d_count
        self.save()
        return rating

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-create_date']
