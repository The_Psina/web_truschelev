from django.contrib import admin
from app.include.models.Answer import Answer
from app.include.models.Question import Question
from app.models import Profile, Like, Dislike, Tag

# Register your models here.
admin.site.register(Tag)
admin.site.register(Question)
admin.site.register(Answer)
admin.site.register(Profile)
admin.site.register(Like)
admin.site.register(Dislike)
