from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator
from askme.settings import ITEMS_ON_PAGE


from app.include.views.authentication_views import *
from app.include.views.forum_views import *

# Create your views here.
