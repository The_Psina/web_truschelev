from django.core.management.base import BaseCommand
from app.models import Profile, Like, Dislike, Tag
from app.include.models.Question import Question
from app.include.models.Answer import Answer

from random import choice, randint
from faker import Faker

import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'askme.settings')

f = Faker()


def fill_questions(cnt):
    author_ids = list(
        Profile.objects.values_list(
            'id', flat=True
        )
    )
    for i in range(cnt):
        Question.objects.create(
            author_id=choice(author_ids),
            text='. '.join(f.sentences(f.random_int(min=2, max=5))),
            title=f.sentence()[:128],
            rating=f.random_int(min=-100, max=100),
            dislikes=f.random_int(min=0, max=50),
        )


def fill_authors(cnt):
    for i in range(cnt):
        Profile.objects.create(
            rating=f.random_int(min=-100, max=100),
            username=f.name()
        )


def fill_likes():
   author_ids = list(
       Profile.objects.values_list(
           'id', flat=True
       )
   )
   question_ids = list(
       Question.objects.values_list(
           'id', flat=True
       )
   )
   for aid in author_ids:
       for qid in question_ids:
           answers = Answer.objects.filter(question_id=qid, )
           profile = Profile.objects.get(id=aid)
           for answer in answers.all():
               answer.like(profile)
           question = Question.objects.get(id=qid)
           question.like(profile)


def set_likes_and_dislikes():
    profile_ids = list(
        Profile.objects.values_list(
            'id', flat=True
        )
    )
    question_ids = list(
        Question.objects.values_list(
            'id', flat=True
        )
    )
    for qid in question_ids:
        answers = Answer.objects.filter(question_id=qid, )
        for answer in answers.all():
            answer.calculate_rating()
            for i in range(f.random_int(min=0, max=20)):
                chance = randint(0, 1)
                if chance == 0:
                    answer.like(Profile.objects.get(id=choice(profile_ids)))
                else:
                    answer.dislike(Profile.objects.get(id=choice(profile_ids)))

        question = Question.objects.get(id=qid)
        question.calculate_rating()
        for i in range(f.random_int(min=0, max=20)):
            chance = randint(0, 1)
            if chance == 0:
                question.like(Profile.objects.get(id=choice(profile_ids)))
            else:
                question.dislike(Profile.objects.get(id=choice(profile_ids)))


def fill_tags(cnt):
    taglist = ['python', 'perl', 'django', 'Technopark', 'mysql', 'mailru']

    question_ids = list(
        Question.objects.values_list(
            'id', flat=True
        )
    )
    for qid in question_ids:
        question = Question.objects.get(id=qid)
        for i in range(f.random_int(min=0, max=cnt)):
            tag = choice(taglist)
            if tag in question.tags.values_list(
                    'name',
                    flat=True
            ):
                continue
            question.tags.create(
                name=tag,
            )


def fill_answers(cnt_per_page):
    author_ids = list(
        Profile.objects.values_list(
            'id', flat=True
        )
    )
    question_ids = list(
        Question.objects.values_list(
            'id', flat=True
        )
    )
    for id in question_ids:
        for j in range(cnt_per_page):
            a = Answer.objects.create(
                question_id=id,
                author_id=choice(author_ids),
                text='. '.join(f.sentences(f.random_int(min=2, max=5))),
                rating=f.random_int(min=-100, max=100),
                dislikes=f.random_int(min=0, max=50),
            )
            a.question.total_answers += 1
            a.question.save()


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--authors', type=int)
        parser.add_argument('--questions', type=int)
        parser.add_argument('--answers', type=int)
        parser.add_argument('--likes')
        parser.add_argument('--fakerate')
        parser.add_argument('--tags', type=int)

    def handle(self, *args, **options):
        if options.get('authors') is not None: fill_authors(options.get('authors'))
        if options.get('questions') is not None: fill_questions(options.get('questions'))
        if options.get('answers') is not None: fill_answers(options.get('answers'))
        if options.get('likes') is not None: set_likes_and_dislikes()
        if options.get('tags') is not None: fill_tags(options.get('tags'))
