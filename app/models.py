from django.db import models
from django.contrib.auth.models import User, AbstractUser

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

from datetime import datetime

from app.utils.like_dislike import *


class Profile(AbstractUser):
    avatar = models.ImageField(upload_to='static/img/', default='static/img/some_avatar.jpg')
    rating = models.IntegerField(default=0)

    def get_avatar(self):
        return self.avatar


class Like(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    users = models.ManyToManyField(Profile)

    def total(self):
        return self.users.count()


class Dislike(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    users = models.ManyToManyField(Profile)

    def total(self):
        return self.users.count()


class Tag(models.Model):
    name = models.CharField(max_length=64, verbose_name='Tag')
    questions_number = models.IntegerField(default=0)

    def __str__(self):
        return self.name

# Create your models here.
